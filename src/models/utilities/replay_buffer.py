#!/usr/env/env python3
import os

class ReplayBuffer:
    def __init__(self, capacity: int=2000000):
        """
        Replay buffer

        Args:
            capacity: number of samples to store
        """
        self.capacity = capacity
        self.replay_buffer = [None] * capacity
        self.current_index = 0
        self.size = 0

    def __len__(self):
        return self.size

    def add_samples(self, samples):
        """
        Add samples to replay buffer

        Replay buffer

        Args:
            capacity: number of samples to store
        """
        import pdb; pdb.set_trace()

if __name__ == "__main__":
    rb = ReplayBuffer()