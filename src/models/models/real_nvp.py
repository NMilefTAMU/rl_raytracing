#!/usr/bin/env python3
import torch

from src.models.models.unet import UNet

# Implementation of the coupling layer
class CouplingLayer(torch.nn.Module):
    def __init__(self, mask):
        super(CouplingLayer, self).__init__()
        self.mask = mask
        self.m = UNet(d_in=2)

    def forward(self, x, direction=1):
        x = self.m(x)
        return x
    
class RealNVP(torch.nn.Module):
    def __init__(self, n_layers=2):
        super(RealNVP, self).__init__()

        self.n_layers = n_layers
        self.layers = torch.nn.ModuleList()
        masks = [[0, 1], [1, 0]]

        for i in range(self.n_layers):
            mask = masks[i % 2]
            layer = CouplingLayer(mask)
            self.layers.append(layer)

    def forward(self, x, px):
        import pdb; pdb.set_trace()
        for i in range(self.n_layers):
            x = self.layers[i](x)

        return x, px