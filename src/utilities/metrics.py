#!/usr/bin/env python3
import math
import numpy as np

def RootMSE(x, y):
    diff = x - y
    error = math.sqrt(np.mean(diff * diff))
    return error

if __name__ == "__main__":
    x = np.zeros((128, 128, 1))
    y = np.ones((128, 128, 1))
    RootMSE(x, y)