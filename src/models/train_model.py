#!/usr/bin/env python3
import argparse
import os
import pdb

from renderers.ns_renderer import NSRenderer # must come before mitsuba import
from src.models.bsdf.bsdf_diffuse import BSDF_Diffuse 

import mitsuba
mitsuba.set_variant('gpu_rgb')
from mitsuba.core import FileStream, Thread
from mitsuba.core import Float, UInt64, Vector2f, Vector3f, PCG32
from mitsuba.core.xml import load_file
from mitsuba.render import Integrator, register_integrator, register_bsdf


def setup_renderer(scene):
    """Setups up Mitsuba renderer for ray tracing"""
    #register_integrator("ns_integrator", lambda props: NSIntegrator(props))
    register_bsdf("bsdf_diffuse", lambda props: BSDF_Diffuse(props))

    filename = scene
    Thread.thread().file_resolver().append(os.path.dirname(filename))
    scene = load_file(filename)

    renderer = NSRenderer(scene)
    renderer.render()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--scene", "-s", type=str, help="path to scene.xml", required=True)
    parser.add_argument("-spp", type=int, help="samples per pixel (spp)", default=4)
    args = parser.parse_args()

    setup_renderer(args.scene)

if __name__ == "__main__":
    main()