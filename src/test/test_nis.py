#!/usr/bin/env python3
import argparse
import numpy as np
import os
import random
from functools import partial
import pdb

from PIL import Image
from matplotlib import pyplot as plt
import torch
from src.utilities.metrics import RootMSE
from src.utilities.loss_plot import LossPlot

from zunis.integration import Integrator
from zunis.models.flows.sampling import UniformSampler
from zunis.integration.fixed_sample_integrator import FixedSampleSurveyIntegrator
from zunis.training.weighted_dataset.stateful_trainer import StatefulTrainer
from zunis.utils.config.loaders import create_integrator_args
from zunis.utils.config.generators import create_integrator_config_file


device = torch.device('cuda')

def test_integration():
    d = 2

    def f(x):
        x = x.cpu().numpy()
        x = np.abs(x[:,0] + x[:,1])
        x = torch.from_numpy(x).to(device)
        return x

    trainer = StatefulTrainer(d=d, device=device, loss="dkl")

    for i in range(100):
        x, px, fx = trainer.generate_target_batch_from_posterior(10, f, UniformSampler(d=d, device=device))
        pdb.set_trace()
        data = trainer.train_on_batch(x, px, fx)
        integrator = FixedSampleSurveyIntegrator(f, trainer, device=device, n_points_survey=5)
        sample = x, px, fx
        integrator.set_sample(sample)
        result, uncertainty, history = integrator.integrate()
        #print(x, px, fx)
        print(result, uncertainty)


def test_image(image_filename):
    os.makedirs("results/test_nis/", exist_ok=True)
    image_name = image_filename.split(".png")[0].split("/")[-1]
    image = np.asarray(Image.open(image_filename)) / 255.0

    loss_plot = LossPlot()

    samples = []
    n_scale = np.mean(image[:,:,0])#1.0 / np.sum(image[:,:,0])

    image = image * n_scale

    for x in range(image.shape[0]):
        for y in range(image.shape[1]):
            nx = x / (image.shape[0] - 1)
            ny = y / (image.shape[1] - 1)
            l = image[x, y, 0]
            samples.append([nx, ny, l])

    num_pixels = len(samples)

    optim = partial(torch.optim.Adam, lr=0.00001)
    trainer = StatefulTrainer(d=2, device=device, loss="dkl", flow="pwquad", n_epochs=1, flow_options={"network_type": "unet"}, optim=optim)
    scheduler = torch.optim.lr_scheduler.StepLR(trainer.config["optim"], 1000, 0.5)

    num_epochs = 10000
    batch_size = 16384
    total_f = np.sum(image)

    filename = "results/test_nis/" + image_name + "_original.png"
    print("Saved", filename)
    Image.fromarray(np.uint8(image[:,:,0]*255), "L").save(filename)
    iterations = 0

    # Train
    for n in range(num_epochs):
        loss = 0
        samples_shuffled = samples.copy()
        random.shuffle(samples_shuffled)

        count = 0
        while count < len(samples_shuffled):
            x = np.zeros((batch_size, 2))
            px = np.zeros((batch_size))
            fx = np.zeros((batch_size))

            for i in range(batch_size):
                x[i,0] = samples_shuffled[count][0]
                x[i,1] = samples_shuffled[count][1]
                px[i] = 1.0 #/ num_pixels
                fx[i] = samples_shuffled[count][2]
                count += 1

            x = torch.from_numpy(x).to(device).float()
            px = torch.from_numpy(px).to(device).float()
            fx = torch.from_numpy(fx).to(device).float()

            record = trainer.train_on_batch(x, px, fx)
            scheduler.step()
            loss += record.loss
            iterations += 1

        x = np.zeros((batch_size, 2))
        px = np.zeros((batch_size))
        fx = np.zeros((batch_size))

        count = 0

        print("Trained for epoch", str(n), "and iterations", str(iterations), "\tloss:", str(loss))
        loss_plot.add_data(iterations, loss)
        loss_plot.save_plot("results/test_nis/" + image_name + "_plot.png")

        output_image = np.zeros((image.shape[0], image.shape[1], 1))
        for i_x in range(image.shape[0]):
            x = np.zeros((image.shape[1], 2))
            px = np.zeros((image.shape[1]))
            for i_y in range(image.shape[1]):
                # Reconstruct
                x[i_y, 0] = i_x / (image.shape[0] - 1)
                x[i_y, 1] = i_y / (image.shape[1] - 1)
                px[i_y] = 1.0 #/ num_pixels

            x = torch.from_numpy(x).to(device).float()
            px = torch.from_numpy(px).to(device).float()

            #trainer.flow.invert()
            output = trainer.flow(torch.cat((x, px.unsqueeze(-1)), -1))
            #trainer.flow.invert()

            for i_y in range(image.shape[1]):
                pos = output.detach().cpu().numpy()[i_y]
                output_image[i_x, i_y, 0] = abs(pos[-1])

        display_results = False
        if display_results:
            plt.gray()
            plt.imshow(output_image.squeeze(), interpolation="nearest")
            plt.show()

        filename = "results/test_nis/" + image_name + "_trained_" + str(n).zfill(8) + ".png"
        if iterations % 10 == 0:
            Image.fromarray(np.uint8(output_image.squeeze()*255), "L").save(filename)
            print("Saved", filename)

            #error = RootMSE(output_image, image[:,:,0])
            #print("Error", error)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--images", "-i", nargs="+", help="images to train on")
    args = parser.parse_args()

    #test_integration()
    for image in args.images:
        test_image(image)