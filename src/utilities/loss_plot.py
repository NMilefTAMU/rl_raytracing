#!/usr/bin/env python3
from matplotlib import pyplot as plt

class LossPlot():
    def __init__(self):
        self.x = []
        self.y = []

    def add_data(self, x, y):
        self.x.append(x)
        self.y.append(y)

    def save_plot(self, filename):
        plt.plot(self.x, self.y, color="blue")
        plt.xlabel("iterations")
        plt.ylabel("loss")
        plt.savefig(filename, dpi=300, bbox_inches="tight")