# Reinforcement Learning Ray Tracing

# Compilation Instructions

To compile this library, a few things must be installed first:

## Mitsuba 2

Install Mitsuba 2 (version 1.0.0). We use the "scalar_rgb" variant, so make sure that variant is compiled. Once it is compiled, run this command:

For Linux/Mac:
```
source setpath.sh
```

For Window:
```
C:/.../mitsuba2> setpath
```

# Training

To train the neural network, use the following:
```
cd ~/<repo_dir>
python src/models/train_model.py
```