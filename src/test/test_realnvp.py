#!/usr/bin/env python3
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
from PIL import Image

from src.models.models.real_nvp import RealNVP

def main():
    image = np.array(Image.open("../images/einstein.png"))[..., 0:1]

    num_samples = image.shape[0] * image.shape[1]

    x = np.zeros((num_samples, 2))
    px = np.ones(num_samples)
    fx = np.zeros((num_samples))

    # populate data
    count = 0
    for i_x in range(image.shape[0]):
        for i_y in range(image.shape[1]):
            fx[count] = image[i_x, i_y]
            x[count, 0] = i_x
            x[count, 1] = i_y
            count += 1

    # Initialize model
    model = RealNVP(n_layers=2)
    loss_function = torch.nn.KLDivLoss()
    optimizer = optim.Adam(model.parameters(), lr=0.001)

    num_epochs = 100

    for i in range(num_epochs):
        tensor_x = torch.from_numpy(x).float()
        tensor_px = torch.from_numpy(px).float()
        tensor_fx = torch.from_numpy(fx).float()

        optimizer.zero_grad()

        output_x, output_px = model(tensor_x, tensor_px)
        loss = loss_function(output_px, tensor_fx)
        loss.backward()
        optimizer.step()

        print("Loss", loss)

if __name__ == "__main__":
    main()