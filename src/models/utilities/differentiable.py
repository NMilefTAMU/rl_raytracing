#!/usr/bin/env python3
import math
import torch
import numpy as np


def gaussian(x: float, mu: torch.Tensor, sigma: float):
    t_sigma = torch.ones(mu.shape) * sigma

    a = 1.0 / (t_sigma * math.sqrt(2 * np.pi))
    b = torch.exp(-(0.5) * (torch.pow(x - mu, 2) / torch.pow(t_sigma, 2)))
    return a * b


def one_blob_encode(tensor: torch.Tensor, bins: int, sigma: float=None) -> torch.Tensor:
    """
    Performs one-blob encoding. Not an efficient function as designed to be used soley for input pre-processing.

    Args:
        tensor: input tensor of continuous values
        bins: number of bins to break down the continuous value into
        sigma: controls the spread of the value into contiguous bins
    Returns:
        torch.Tensor: blob encoding
    """
    if not sigma:
        sigma = 1.0 / bins

    initial_shape = list(tensor.size())
    initial_shape[-1] *= bins
    encoded_data = torch.zeros(initial_shape)

    for d in range(tensor.shape[-1]):
        for k in range(bins):
            x_offset = k / (bins - 1)
            encoded_data[:, d * bins + k] = gaussian(x_offset, tensor[d], sigma)

    return encoded_data


if __name__ == "__main__":
    # Test gaussian
    mean = torch.zeros((4, 1))
    print(gaussian(0.7, mean, math.sqrt(0.2)))

    # Test one-blob encoding
    data = torch.ones((4, 1)) * 0.11
    encoded_data = one_blob_encode(data, 32)
    print(encoded_data)
