#!/usr/bin/env python3
import torch
import torch.nn as nn

class UNet(nn.Module):
    def __init__(self, d_in):
        super(UNet, self).__init__()
        self.layers = nn.ModuleList()
        self.layers.append(nn.Linear(d_in, 256))
        self.layers.append(nn.ReLU())
        self.layers.append(nn.Linear(256, 128))
        self.layers.append(nn.ReLU())
        self.layers.append(nn.Linear(128, 64))
        self.layers.append(nn.ReLU())
        self.layers.append(nn.Linear(64, 32))
        self.layers.append(nn.ReLU())
        self.layers.append(nn.Linear(32, 16))
        self.layers.append(nn.ReLU())
        self.layers.append(nn.Linear(16, 32))
        self.layers.append(nn.ReLU())
        self.layers.append(nn.Linear(32, 64))
        self.layers.append(nn.ReLU())
        self.layers.append(nn.Linear(64, 128))
        self.layers.append(nn.ReLU())
        self.layers.append(nn.Linear(128, 256))
        self.layers.append(nn.ReLU())
        self.layers.append(nn.Linear(256, 1))

    def forward(self, x):
        x_d_256     = self.layers[0](x)
        x_d_256a    = self.layers[1](x_d_256)
        x_d_128     = self.layers[2](x_d_256a)
        x_d_128a    = self.layers[3](x_d_128)
        x_d_64      = self.layers[4](x_d_128a)
        x_d_64a     = self.layers[5](x_d_64)
        x_d_32      = self.layers[6](x_d_64a)
        x_d_32a     = self.layers[7](x_d_32)
        x_d_16      = self.layers[8](x_d_32a)
        x_d_16a     = self.layers[9](x_d_16)
        x_u_32      = self.layers[10](x_d_16a) + x_d_32
        x_u_32a     = self.layers[11](x_u_32)
        x_u_64      = self.layers[12](x_u_32a) + x_d_64
        x_u_64a     = self.layers[13](x_u_64)
        x_u_128     = self.layers[14](x_u_64a) + x_d_128
        x_u_128a    = self.layers[15](x_u_128)
        x_u_256     = self.layers[16](x_u_128a) + x_d_256
        x_u_256a    = self.layers[17](x_u_256)
        output      = self.layers[18](x_u_256a)

        import pdb; pdb.set_trace()
        return output