import os
#from src.models.utilities.replay_buffer import ReplayBuffer
import torch
import enoki as ek
import numpy as np
from PIL import Image

# Import neural importance sampling libraries
from zunis.integration import Integrator
from zunis.training.weighted_dataset.stateful_trainer import StatefulTrainer
from zunis.utils.config.loaders import create_integrator_args
from zunis.utils.config.generators import create_integrator_config_file

# Import Mitsuba custom classes
from src.models.bsdf.bsdf_diffuse import BSDF_Diffuse

# Import Mitsuba libraries
import mitsuba
mitsuba.set_variant('gpu_rgb')
from mitsuba.core import Vector1f, Vector2f, Vector3f, Thread, Float, Properties, xml, Float32, UInt32, Vector2f, warp
from mitsuba.core.xml import load_file
from mitsuba.render import (BSDF, BSDFSample3f, BSDFContext, BSDFFlags, DirectionSample3f,
                              Emitter, ImageBlock, SamplingIntegrator,
                              SamplingIntegrator, has_flag)

# Adapted from "direct_integrator.py" from "03_direct_integrator" from Mitsuba2 examples
# Original source code under MIT license
def mis_weight(pdf_a, pdf_b):
    pdf_a *= pdf_a
    pdf_b *= pdf_b
    return ek.select(pdf_a > 0.0, pdf_a / (pdf_a + pdf_b), Float(0.0))

def format_input(si: Vector3f, wo: Vector3f):
    """
    Formats input as Numpy array

    Args:
        si: surface intersection
        wo: outgoing light

    Returns:
        numpy.array: input concatentated together
    """
    x = si.p.numpy()    # position
    n = si.n.numpy()    # surface normal
    wi = si.wi.numpy()  # incoming light
    input_data = np.concatenate((x, n, wi), axis=1)
    return input_data

class NSRenderer():
    def __init__(self, scene):
        self.scene = scene
        self.sensor = self.scene.sensors()[0]
        self.film = self.sensor.film()
        self.film_size = self.film.crop_size()
        self.sampler = self.sensor.sampler()

        self.batch_size = 102400 // 8
        self.spp = 2
        self.level_sample_count = ek.hprod(self.film_size)
        self.total_sample_count = self.level_sample_count * self.spp

        #self.sampler_bsdf = BSDF_Diffuse(Properties())

        self.output = np.zeros((int(self.film_size[0]), int(self.film_size[1]), 3))

        self.device = torch.device('cuda')

        self.trainer = StatefulTrainer(d=2, device=self.device)

        #self.replay_buffer = ReplayBuffer(2000000)

    def render(self):
        total_rays = 0
        rays_left = self.total_sample_count

        while rays_left > 0:
            print(total_rays)
            level = total_rays // self.level_sample_count
            offset = total_rays % self.level_sample_count

            batch_rays = min(self.batch_size, rays_left)
        
            if self.sampler.wavefront_size() != batch_rays:
                self.sampler.seed(0, batch_rays)

            pos = ek.arange(UInt32, batch_rays) + offset
            scale = Vector2f(1.0 / self.film_size[0], 1.0 / self.film_size[1])
            pos = Vector2f(Float(pos % int(self.film_size[0])),
                Float(pos // int(self.film_size[0])))

            pos_int = pos
            pos += self.sampler.next_2d()

            rays, weights = self.sensor.sample_ray_differential(
                time = 0,
                sample1 = self.sampler.next_1d(),
                sample2 = pos * scale,
                sample3 = 0
            )

            result_temp, is_valid, done = self.integrator_sample(self.scene, self.sampler, rays, None, 1, [1], nis_integrator=True)

            pos_index = pos_int.numpy().astype(np.int)
            indices = list(map(tuple, pos_index))
            rows, cols = zip(*indices)

            # Blend previous result with current samples
            alpha_0 = level / (level+1)
            alpha_1 = 1.0 - alpha_0
            self.output[rows, cols] = (alpha_0 * self.output[rows, cols]) + (alpha_1 * result_temp.numpy())

            rays_left -= batch_rays
            total_rays += batch_rays

        resultOutput = (np.flipud(np.rot90(self.output, k=1, axes=(0,1)))*255).astype(np.uint8)
        image = Image.fromarray(resultOutput).convert('RGB')
        image.save('test2.png')

    def sample_pss(self, bsdf, ctx, si, sample, active):
        data = self.sampler_bsdf.sample(ctx, si, None, sample, active)
        return data[0], data[1]

    def gen_sample(self, x, px):
        self.trainer.flow.invert()
        output = self.trainer.flow(torch.cat((x, px.unsqueeze(-1)), -1))
        self.trainer.flow.invert()
        return output.detach().cpu().numpy()

    # Adapted from "direct_integrator.py" from "03_direct_integrator" from Mitsuba2 examples
    # Original source code under MIT license
    def integrator_sample(self, scene, sampler, rays, medium, maxDepth, active=True, nis_integrator=None):
        depth = 0
        si = scene.ray_intersect(rays)
        si_bsdf = None
        result = None
        emission_weight = Float(1)
        throughput = Float(1)

        training_data = {"x": [], "px": [], "fx": None}

        while depth < maxDepth:
            if si_bsdf:
                si = si_bsdf

            active = si.is_valid() & active

            # Visible emitters
            emitter_vis = si.emitter(scene, active)
            emitter_val = emission_weight * throughput * ek.select(active, Emitter.eval_vec(emitter_vis, si, active), Vector3f(0.0))
            if not result:
                result = emitter_val
            else:
                result += emitter_val

            ctx = BSDFContext()
            bsdf = si.bsdf(rays)

            # -------------------------------- Emitter sampling --------------------------------
            sample_emitter = active & has_flag(BSDF.flags_vec(bsdf), BSDFFlags.Smooth)
            ds, emitter_val = scene.sample_emitter_direction(si, sampler.next_2d(sample_emitter), True, sample_emitter)
            active_e = sample_emitter & ek.neq(ds.pdf, 0.0)

            # Query the BSDF for that emitter-sampled direction
            wo = si.to_local(ds.d)
            input_data = format_input(si, wo)
            bsdf_val = BSDF.eval_vec(bsdf, ctx, si, wo, active_e)

            # Determine the density of sampling that same direction using BSDF sampling
            #if nis_integrator and depth == 0:
                #bsdf_pdf = self.trainer.sample_forward()
            #else:
            bsdf_pdf = BSDF.pdf_vec(bsdf, ctx, si, wo, active_e)

            mis = ek.select(ds.delta, Float(1), mis_weight(ds.pdf, bsdf_pdf))
            #result += ek.select(active_e, emitter_val * bsdf_val * mis, Vector3f(0))
            result += mis * throughput * bsdf_val * emitter_val

            # -------------------------------- BSDF sampling --------------------------------
            active_b = active

            # Add logic here
            sampler_1d_values = np.zeros((active.numpy().shape[0]))#sampler.next_1d(active) # for lobe selection
            sampler_2d_values = sampler.next_2d(active).numpy() # for PSS selection
            sample_pdf = np.ones((sampler_2d_values.shape[0]))
            #sampler_2d_values = self.gen_sample(
                #torch.from_numpy(sampler_2d_values).float().to(self.device),
                #torch.from_numpy(sample_pdf).float().to(self.device))

            bs, bsdf_val = BSDF.sample_vec(bsdf, ctx, si, Float32(sampler_1d_values), Vector2f(sampler_2d_values), active_b)
            #bs, bsdf_val = self.sample_pss(bsdf, ctx, si, sampler_2d_values, active)
            bsdf_val = si.to_world_mueller(bsdf_val, -bs.wo, si.wi)
            throughput = throughput * bsdf_val

            # intersect the BSDF ray against the scene geometry
            bsdf_rays = si.spawn_ray(si.to_world(bs.wo))
            si_bsdf = scene.ray_intersect(bsdf_rays, active_b)
            #import pdb; pdb.set_trace()

            # Determine probability of having sampled that same direction using emitter sampling
            emitter = si_bsdf.emitter(scene, active_b)
            active_b &= ek.neq(emitter, 0)
            emitter_val = Emitter.eval_vec(emitter, si_bsdf, active_b)
            delta = has_flag(bs.sampled_type, BSDFFlags.Delta)
            ds = DirectionSample3f(si_bsdf, si)
            ds.object = emitter
            emitter_pdf = ek.select(delta, Float(0), scene.pdf_emitter_direction(si, ds, active_b))
            emission_weight = mis_weight(bsdf_pdf, emitter_pdf)

            result += ek.select(active_b, bsdf_val * emitter_val * mis_weight(bs.pdf, emitter_pdf), Vector3f(0))

            #if nis_integrator and depth == 0:
                #self.replay_buffer.add_samples((input_data, result.numpy()))

            depth += 1
            training_data["x"].append(sampler_2d_values)
            training_data["fx"] = throughput

        #x = torch.from_numpy(np.concatenate(training_data["x"], axis=1)[:,0:2]).to(self.device)
        #px = torch.from_numpy(np.concatenate(training_data["x"], axis=1)[:,2]).to(self.device)
        #fx = torch.from_numpy(np.expand_dims(throughput.numpy(), axis=1)[:,0,0]).to(self.device)

        #self.trainer.train_on_batch(x, px, fx)

        return result, si.is_valid(), ek.select(si.is_valid(), si.t, Float(0.0))

    # Adapted from "direct_integrator.py" from "03_direct_integrator" from Mitsuba2 examples
    # Original source code under MIT license
    def integrator_sample_pss(self, scene, sampler, rays, medium, maxDepth, active=True, nis_integrator=None):
        depth = 0
        si = scene.ray_intersect(rays)
        si_bsdf = None
        result = None
        emission_weight = Float(1)
        throughput = Float(1)

        while depth < maxDepth:
            if si_bsdf:
                si = si_bsdf

            active = si.is_valid() & active

            ctx = BSDFContext()
            bsdf = si.bsdf(rays)

            # -------------------------------- BSDF sampling --------------------------------
            active_b = active

            # Add logic here
            sampler_1d_values = sampler.next_1d(active)
            sampler_2d_values = sampler.next_2d(active)

            bs, bsdf_val = BSDF.sample_vec(bsdf, ctx, si, sampler_1d_values, sampler_2d_values, active_b)
            #bs, bsdf_val = self.sample_pss(bsdf, ctx, si, sampler_2d_values, active)
            bsdf_val = si.to_world_mueller(bsdf_val, -bs.wo, si.wi)
            #bsdf_pdf = self.sampler_bsdf.pdf(ctx, si, bs.wo, active)
            throughput = throughput * bsdf_val

            # intersect the BSDF ray against the scene geometry
            bsdf_rays = si.spawn_ray(si.to_world(bs.wo))
            si_bsdf = scene.ray_intersect(bsdf_rays, active_b)
            bsdf_pdf = BSDF.pdf_vec(bsdf, ctx, si, bs.wo, active)

            # Determine probability of having sampled that same direction using emitter sampling
            emitter = si_bsdf.emitter(scene, active_b)
            active_b &= ek.neq(emitter, 0)
            emitter_val = Emitter.eval_vec(emitter, si_bsdf, active_b)
            delta = has_flag(bs.sampled_type, BSDFFlags.Delta)
            ds = DirectionSample3f(si_bsdf, si)
            ds.object = emitter
            emitter_pdf = ek.select(delta, Float(0), scene.pdf_emitter_direction(si, ds, active_b))
            emission_weight = mis_weight(bsdf_pdf, emitter_pdf)
            #mis_ = mis_weight(bs.pdf, emitter_pdf)

            result_val = ek.select(active_b, bsdf_val * emitter_val * emission_weight, Vector3f(0))
            if result is None:
                result = result_val
            else:
                result += result_val
            #import pdb; pdb.set_trace()

            #if nis_integrator and depth == 0:
                #self.replay_buffer.add_samples((input_data, result.numpy()))

            depth += 1

        return result, si.is_valid(), ek.select(si.is_valid(), si.t, Float(0.0))